# Python爬虫，v0.0.1 爬取豆瓣电影top250

- 前置条件
1. Python3.6.4
2. Scrapy

- 运行方式
```
// 去到文件夹下，打开terminal输入
scrapy crawl dmoz
```

- 修改地址需要爬的网站的地址   `/spider/tutorial/tutorial/spiders`  在 `start_urls` 新增想要爬的地址就好
- 数据处理，`/spider/tutorial/tutorial/spiders` 下有个 `parse` 函数，在页面用xpath找到对应的，复制进来即可