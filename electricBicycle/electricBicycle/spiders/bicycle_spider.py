import scrapy
from electricBicycle.items import ElectricbicycleItem
# from items import TutorialItem
class DmozSpider(scrapy.Spider):
    name = "bicycle"
    allowed_domains = ["detail.zol.com.cn"]
    start_urls = [
        "http://detail.zol.com.cn/convenienttravel"
    ]
    def parse(self, response):
        # filename = response.url
        arr = response.xpath('//*[@id="J_PicMode"]/li/a')
        for item in arr:
            url = item.xpath('@href').extract_first()
            yield scrapy.Request("http://detail.zol.com.cn"+url,callback=self.collect)
        next_link = response.xpath('/html/body/div[4]/div[1]/div[9]/div[1]/a[@class="next"]/@href').extract()
        if next_link:
            next_link = next_link[0]
            yield scrapy.Request("http://detail.zol.com.cn"+next_link,callback=self.parse)
    def collect(self,response):
        obj = ElectricbicycleItem()
        path = response.xpath('//dl[@class="product__parameters"]/dd/ul')
        obj['name'] = response.xpath('/html/body/div[9]/h1/text()').extract_first()
        obj['price'] = response.xpath('//b[@class="price-type"]/text()').extract_first()
        obj['renewal'] = path.xpath('li[3]/text()').extract_first()
        obj['speed'] = path.xpath('li[4]/text()').extract_first()
        obj['star'] = response.xpath('//div[@class="review__score"]/strong/text()').extract_first()
        obj['image_urls'] = response.xpath('//div[@class="big-pic"]/a/img/@src').extract_first()
        return obj