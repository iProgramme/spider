# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import json
import scrapy
from scrapy.contrib.pipeline.images import ImagesPipeline
from scrapy.exceptions import DropItem
from scrapy.pipelines.images import ImagesPipeline


class TutorialPipeline(object):
    def __init__(self, *args, **kwargs):
        self.f = open('demo.json','w')
        
    def process_item(self, item, spider):
        self.content.append(dict(item))
        return item
    def close_spider(self,spider):
        self.f.write(json.dumps(self.content,ensure_ascii=False))
        self.f.close()
    def open_spider(self,spider):
        self.content = []

class MyImagesPipeline(ImagesPipeline):

    def get_media_requests(self, item, info):
        # for image_url in item['image_urls']:
        yield scrapy.Request(item['image_urls'])

    def item_completed(self, results, item, info):
        image_paths = [x['path'] for ok, x in results if ok]
        if not image_paths:
            raise DropItem("Item contains no images")
        item['image_paths'] = image_paths
        return item