import scrapy
# from items import TutorialItem
class DmozSpider(scrapy.Spider):
    name = "dmoz"
    allowed_domains = ["movie.douban.com","img3.doubanio.com"]
    start_urls = [
        "https://movie.douban.com/top250",
        # "https://movie.douban.com/top250?start=25&filter="
    ]

    def parse(self, response):
        # filename = response.url
        tutorialItem = {}
        # app = TutorialItem()
        arr = response.xpath('//*[@id="content"]/div/div[1]/ol/li/div')
        for item in arr:
            tutorialItem['number'] = item.xpath('div[1]/em/text()').extract_first()
            tutorialItem['name'] = item.xpath('div[2]/div[1]/a/span[1]/text()').extract_first()
            tutorialItem['star'] = item.xpath('div[2]/div[2]/div[@class="star"]/span[2]/text()').extract_first()
            tutorialItem['judge'] = item.xpath('div[2]/div[2]/div[@class="star"]/span[4]/text()').extract_first()
            tutorialItem['detail'] = item.xpath('div[2]/div[2]/p[@class="quote"]/span/text()').extract_first()
            tutorialItem['image_urls'] = item.xpath('div[1]/a/img/@src').extract_first()
            yield tutorialItem
        next_link = response.xpath('//*[@id="content"]/div/div[1]/div[2]/span[3]/a/@href').extract()
        if next_link:
            next_link = next_link[0]
            yield scrapy.Request("https://movie.douban.com/top250"+next_link,callback=self.parse)
            